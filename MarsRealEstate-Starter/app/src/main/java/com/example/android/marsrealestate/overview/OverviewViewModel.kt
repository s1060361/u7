/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.network.MarsApi
import com.example.android.marsrealestate.network.MarsApiFilter
import com.example.android.marsrealestate.network.MarsProperty
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

//enum代表所有可用狀態
enum class MarsApiStatus { LOADING, ERROR, DONE }
/**
 * The [ViewModel] that is attached to the [OverviewFragment].
 */
class OverviewViewModel : ViewModel() {

    // The external immutable LiveData for the response String
    private val _status = MutableLiveData<MarsApiStatus>()

    val status: LiveData<MarsApiStatus>
        get() = _status

    //添加協程作業
    private var viewModelJob = Job()

    //使用主調度程序為該新作業創建一個協程範圍
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )


    private val _properties = MutableLiveData<List<MarsProperty>>()

    //將外部property實時數據替換為properties。還將列表添加到LiveData此處的類型
    val properties: LiveData<List<MarsProperty>>
        get() = _properties

    /**
     * Call getMarsRealEstateProperties() on init so we can display status immediately.
     */
    init {
        //在應用首次加載時顯示所有屬性
        getMarsRealEstateProperties(MarsApiFilter.SHOW_ALL)
    }

    //當此選項LiveData更改為非null時，將觸發導航
    private val _navigateToSelectedProperty = MutableLiveData<MarsProperty>()
    val navigateToSelectedProperty: LiveData<MarsProperty>
        get() = _navigateToSelectedProperty

    /**
     * Sets the value of the status LiveData to the Mars API status.
     */
    //調用Retrofit服務並處理返回的JSON字符串的方法
    private fun getMarsRealEstateProperties(filter: MarsApiFilter) {
        coroutineScope.launch {
            _status.value = MarsApiStatus.LOADING
            //getProperties()在Retrofit服務中修改對的調用，以將該過濾器查詢作為字符串傳遞。
            var getPropertiesDeferred = MarsApi.retrofitService.getProperties(filter.value)
            try {
                _status.value = MarsApiStatus.LOADING
                //  //由於listResult變量包含MarsProperty對象列表，因此您可以將其分配給它，_properties.value而不是測試是否成功響應
                val listResult = getPropertiesDeferred.await()
                _status.value = MarsApiStatus.DONE
                _properties.value = listResult
            } catch (e: Exception) {
                _status.value = MarsApiStatus.ERROR
                _properties.value = ArrayList()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun updateFilter(filter: MarsApiFilter) {
        getMarsRealEstateProperties(filter)
    }

    //displayPropertyDetails()將_ navigateToSelectedProperty設置為選定的Mars屬性的方法
    fun displayPropertyDetails(marsProperty: MarsProperty) {
        _navigateToSelectedProperty.value = marsProperty
    }

    //使_ navigateToSelectedProperty的值無效
    //需要用它來標記導航狀態完成，並避免當用戶從局部視圖返回時再次觸發導航
    fun displayPropertyDetailsComplete() {
        _navigateToSelectedProperty.value = null
    }



}