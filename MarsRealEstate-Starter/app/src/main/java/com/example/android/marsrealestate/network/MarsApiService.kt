/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

//創建一個enum調用MarsApiFilter以定義與Web服務期望的查詢值匹配的常量
enum class MarsApiFilter(val value: String){
    SHOW_RENT("rent"),
    SHOW_BUY("buy"),
    SHOW_ALL("all")
}

//Web服務的基本URL的常量
private const val BASE_URL = " https://android-kotlin-fun-mars-server.appspot.com/"


private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()


//使用Retrofit構建器創建Retrofit對象。
//etrofit具有ScalarsConverter支持字符串和其他原始類型的，因此您addConverterFactory()使用的實例調用構建器ScalarsConverterFactory。
// 最後調用build()創建Retrofit對象
private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .baseUrl(BASE_URL)
        .build()

//定義Retrofit如何使用HTTP請求與Web服務器對話
//要告訴Retrofit該方法應該做什麼，請使用@GET註釋並指定該Web服務方法的路徑或端點
//端點稱為realestate。getProperties()調用該方法時，Retrofit將端點附加realestate到基本URL
//對Call像用於啟動請求
interface MarsApiService {
    @GET("realestate")
    //與過濾器選項Web服務請求。每次getProperties()調用時，請求URL都包含?filter=type一部分，
    // 該部分指示Web服務以與該查詢匹配的結果進行響應
    fun getProperties(@Query("filter") type: String):
            Deferred<List<MarsProperty>>        //返回MarsProperty對象列表，而不是返回Call<String>
}

//定義一個公共對象，MarsApi以初始化Retrofit服務
object MarsApi {
    val retrofitService : MarsApiService by lazy {
        retrofit.create(MarsApiService::class.java) }
}
