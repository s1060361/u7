/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate

import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.android.marsrealestate.network.MarsProperty
import com.example.android.marsrealestate.overview.MarsApiStatus
import com.example.android.marsrealestate.overview.PhotoGridAdapter

//@BindingAdapter註解告訴數據綁定要執行此綁定適配器時的XML項具有imageUrl屬性
@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    //let {}為imgUrl參數添加一個塊
    imgUrl?.let {
        //將URL字符串（來自XML）轉換為Uri對象
        //要使用HTTPS方案，請附加buildUpon.scheme("https")到toUri構建器。
        // 該toUri()方法是Android KTX核心庫中的Kotlin擴展函數，因此看起來就像是String類的一部分
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()

        //代碼設置佔位符加載圖像，以在加載時使用（loading_animation可繪製對象
        //該代碼還設置了圖像加載失敗時使用的圖像（broken_image可繪製對象）
        //使用apply()with placeholder()可以指定加載可繪製對象，使用apply()with error()可以指定錯誤可繪製對象
        Glide.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image))
                .into(imgView)
    }
}

//使用BindingAdapter設置RecyclerView數據會使數據綁定自動觀察對象LiveData列表MarsProperty
//然後，當MarsProperty列表更改時，將自動調用綁定適配器
@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<MarsProperty>?) {
    //強制轉換recyclerView.adapter為PhotoGridAdapter，然後調用adapter.submitList()數據。這告訴RecyclerView新列表何時可用
    val adapter = recyclerView.adapter as PhotoGridAdapter
    adapter.submitList(data)
}

//添加一個新的綁定適配器bindStatus()，該適配器以ImageView和MarsApiStatus值作為參數
@BindingAdapter("marsApiStatus")
fun bindStatus(statusImageView: ImageView,status: MarsApiStatus?){
    when (status) {
        MarsApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        MarsApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        MarsApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}

