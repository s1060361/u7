/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.marsrealestate.databinding.GridViewItemBinding
import com.example.android.marsrealestate.network.MarsProperty

//在PhotoGridAdapter類擴展ListAdapter，它的構造需要的列表項的類型，觀點持有者和DiffUtil.ItemCallback實施
class PhotoGridAdapter(private val onClickListener: OnClickListener) : ListAdapter<MarsProperty,
        PhotoGridAdapter.MarsPropertyViewHolder>(DiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoGridAdapter.MarsPropertyViewHolder {

        //onCreateViewHolder() method needs to return a new MarsPropertyViewHolder
        //created by inflating the GridViewItemBinding and using the LayoutInflater from your parent ViewGroup context
        return MarsPropertyViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: PhotoGridAdapter.MarsPropertyViewHolder, position: Int) {
        //調用getItem()以獲取MarsProperty與當前RecyclerView位置相關聯的對象，然後將該屬性傳遞給中的bind()方法
        val marsProperty = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(marsProperty)
        }
        holder.bind(marsProperty)
    }

    //DiffCallback對象擴展DiffUtil.ItemCallback與要compare-對象的類型MarsProperty
    companion object DiffCallback : DiffUtil.ItemCallback<MarsProperty>() {
        override fun areItemsTheSame(oldItem: MarsProperty, newItem: MarsProperty): Boolean {
            //===引用相等，內容及位置必須一樣
            //true如果對象所引用的oldItem和newItem相同，則返回該運算符
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: MarsProperty, newItem: MarsProperty): Boolean {
            return oldItem.id == newItem.id
        }
    }

    //將GridViewItemBinding變量綁定MarsProperty到佈局，因此請將變量傳遞到中MarsPropertyViewHolder
    //因為基ViewHolder類在其構造函數中需要一個視圖，所以您將其傳遞給綁定根視圖
    class MarsPropertyViewHolder(private var binding: GridViewItemBinding): RecyclerView.ViewHolder(binding.root) {
        //創建一個bind()採用一個方法MarsProperty對象作為參數，並設置binding.property到該對象
        //executePendingBindings()設置屬性後調用，這將導致更新立即執行
        fun bind(marsProperty: MarsProperty) {
            binding.property = marsProperty
            binding.executePendingBindings()
        }
    }

    //使用帶有marsProperty參數的lambda 。在類內部，定義一個onClick()設置為lambda參數的函數
    class OnClickListener(val clickListener: (marsProperty:MarsProperty) -> Unit) {
        fun onClick(marsProperty:MarsProperty) = clickListener(marsProperty)

    }
}
