/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.trackmysleepquality.sleeptracker

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.trackmysleepquality.R
import com.example.android.trackmysleepquality.convertDurationToFormatted
import com.example.android.trackmysleepquality.convertNumericQualityToString
import com.example.android.trackmysleepquality.database.SleepNight
import com.example.android.trackmysleepquality.databinding.ListItemSleepNightBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.ClassCastException

private val ITEM_VIEW_TYPE_HEADER = 0
private val ITEM_VIEW_TYPE_ITEM = 1

class SleepNightAdapter(val clickListener: SleepNightListener):ListAdapter<DataItem, RecyclerView.ViewHolder>(SleepNightDiffCallback()) {

    private val adapterScope = CoroutineScope(Dispatchers.Default)

    //此函數採用的列表SleepNight。您可以使用此功能添加標題，然後提交列表，而不是使用submitList()，提供的ListAdapter提交列表
    //如果傳入的列表為null，則僅返回標題，否則，將標題附加到列表的開頭
    //啟動協程adapterScope以操作列表。然後切換到Dispatchers.Main上下文以提交列表
    fun addHeaderAndSubmitList(list: List<SleepNight>?) {
        val items = when(list){
            null -> listOf(DataItem.Header)
            else -> listOf(DataItem.Header) + list.map { DataItem.SleepNightItem(it) }
        }
        withContext(Dispatchers.Main) {
            submitList(items)
        }
    }

    //onBindViewHolder()方法採用兩個參數：視圖持有者和要綁定的數據的位置。
    // 對於此應用程序，持有人為TextItemViewHolder，位置為列表中的位置
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        //添加條件以僅將數據分配給視圖持有人（如果持有人為）ViewHolder
        when(holder){
            is ViewHolder -> {
                //Cast the object type returned by getItem() to DataItem.SleepNightItem
                val nightItem = getItem(position) as DataItem.SleepNightItem
                holder.bind(nightItem.sleepNight,clickListener)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //測試並返回每種項目類型的適當視圖持有者
        return when(viewType){
            ITEM_VIEW_TYPE_HEADER -> TextViewHolder.from(parent)
            ITEM_VIEW_TYPE_ITEM -> ViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType ${viewType}")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.SleepNightItem -> ITEM_VIEW_TYPE_ITEM
        }
    }

    //此類會放大textview.xml佈局，並返回一個實例
    class TextViewHolder(view: View): RecyclerView.ViewHolder(view) {
        companion object {
            fun from(parent: ViewGroup): TextViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.header, parent, false)
                return TextViewHolder(view)
            }
        }
    }

    class ViewHolder private constructor(val binding: ListItemSleepNightBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bind(item: SleepNight) {
            //在內部bind()，將睡眠分配給item您，因為您需要將新對象告知綁定對象SleepNight
            binding.sleep = item
            // //executePendingBindings()當您在中使用綁定適配器時，總是一個好主意RecyclerView，因為它可以稍微加快視圖的大小
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemSleepNightBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

}


class SleepNightDiffCallback : DiffUtil.ItemCallback<DataItem>() {

    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }
}

//sealed類定義一個封閉的類型，這意味著DataItem必須在此文件中定義的所有子類。
// 子類的數量對於編譯器是已知的
sealed class DataItem {
    //在DataItem類主體內，定義兩個代表不同類型數據項的類。第一個是a SleepNightItem
    // 是a的包裝SleepNight，因此它採用一個稱為的值sleepNight。為了使其成為密封類的一部分
    data class SleepNightItem(val sleepNight: SleepNight) : DataItem(){
        override val id = sleepNight.nightId        //覆寫id以返回nightId
    }

    //Header，代表標題。由於標頭沒有實際數據，因此可以將其聲明為object。這意味著將永遠只有一個實例Header
    object Header: DataItem(){
        //重寫id 以返回Long.MIN_VALUE，這是一個非常非常小的數字（從-2到63的冪）。因此，這將永遠不會與任何nightId存在的衝突
        override val id = Long.MIN_VALUE
    }

    //定義一個abstract Long名為的屬性id。當適配器用於DiffUtil確定某個項目是否以及如何更改時
    // DiffItemCallback需要知道每個項目的ID
    abstract val id: Long
}


