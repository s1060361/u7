/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.devbyteviewer.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.android.devbyteviewer.database.VideoDao
import com.example.android.devbyteviewer.database.asDomainModel
import com.example.android.devbyteviewer.domain.DevByteVideo
import com.example.android.devbyteviewer.network.DevByteNetwork
import com.example.android.devbyteviewer.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

/**
 * Repository for fetching devbyte videos from the network and storing them on disk
 */
//創建一個VideosRepository類。傳入對VideosDatabase像作為類的構造函數參數以訪問Dao方法
class VideosRepository(private val database: VideoDao.VideosDatabase) {

    val videos: LiveData<List<DevByteVideo>> = Transformations.map(database.videoDao.getVideo()) {
        it.asDomainModel()
    }

    //refreshVideos()沒有參數且不返回任何內容的方法。此方法將是用於刷新脫機緩存的API
    //請refreshVideos()暫停功能。因為refreshVideos()執行數據庫操作，所以必須從協程調用它
    suspend fun refreshVideos(){
        //切換協程上下文Dispatchers.IO以執行網絡和數據庫操作
        withContext(Dispatchers.IO){
            //帶有用於跟踪何時被調用的日誌語句
            Timber.d("refresh videos is called");

            //使用改造服務實例從網絡中獲取DevByte視頻播放列表DevByteNetwork。
            // 使用await()功能可暫停協程，直到播放列表可用為止
            val playlist = DevByteNetwork.devbytes.getPlaylist().await()

            //使用VideosDatabase對象database存儲播放列表
            // 調用insertAllDAO方法，傳入playlist從網絡中檢索到的方法。
            // 使用asDatabaseModel()擴展功能將映射playlist到數據庫對象。
            database.videoDao.insertAll(playlist.asDatabaseModel())
        }
    }
}