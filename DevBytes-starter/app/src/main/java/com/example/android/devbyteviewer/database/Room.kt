/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.devbyteviewer.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface VideoDao{
    //getVideo從資料庫中獲取所有視頻
    // 將方法返回類型更改為LivaData以便每當更改數據庫中的數據食，都獲刷新UI中顯示的數據
    @Query("select * from databasevideo")
    fun getVideo(): LiveData<List<DatabaseVideo>>

    //InsertAll將從網絡獲取的視頻列表插入數據庫
    //使用onConflict參數將衝突策略設置為REPLACE(如果視頻條目已經存在於數據庫中，則覆蓋數據庫條目)
    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insertAll(video: List<DatabaseVideo>)

    //創建一個abstract稱為類VideosDatabase。VideosDatabase從擴展RoomDatabase
    //使用@Database註釋將VideosDatabase類標記為Room數據庫。聲明DatabaseVideo屬於該數據庫的實體，並將版本號設置為1
    //在內部VideosDatabase，定義類型的變量VideoDao以訪問Dao方法
    @Database(entities = [DatabaseVideo::class], version = 1)
    abstract class VideosDatabase: RoomDatabase(){
        abstract val videoDao: VideoDao
    }
}

private lateinit var INSTANCE: VideoDao.VideosDatabase

fun getDatabase(context: Context): VideoDao.VideosDatabase {
    synchronized(VideoDao.VideosDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                    VideoDao.VideosDatabase::class.java,
                    "videos").build()
        }
    }
    return INSTANCE
}