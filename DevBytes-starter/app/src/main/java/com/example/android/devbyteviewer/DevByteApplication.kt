/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.devbyteviewer

import android.app.Application
import android.os.Build
import androidx.work.*
import com.example.android.devbyteviewer.work.RefreshDataWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.TimeUnit

private val applicationScope = CoroutineScope(Dispatchers.Default)


/**
 * Override application to setup background work via WorkManager
 */
class DevByteApplication : Application() {

    private val applicationScope = CoroutineScope(Dispatchers.Default)
    /**
     * onCreate is called before the first screen is shown to the user.
     *
     * Use it to setup any background tasks, running expensive setup operations in a background
     * thread to avoid delaying app start.
     */
    override fun onCreate() {
        super.onCreate()
        delayedInit()
    }

    private fun delayedInit() {
        applicationScope.launch {
            Timber.plant(Timber.DebugTree())
            setupRecurringWork()
        }
    }

    /**
     * Setup WorkManager background job to 'fetch' new network data daily.
     */
    //setupRecurringWork()設置重複的後台工作
    private fun setupRecurringWork() {

        //創建一個Constraints對象，並在該對像上設置一個約束，即網絡類型約束
        val constraints = Constraints.Builder()
                //使用setRequiredNetworkType()向對象添加網絡類型的約束constraints
                //使用UNMETERED以便僅在設備楚瑜為劑量網路上時運行工作請求
                .setRequiredNetworkType(NetworkType.UNMETERED)
                .setRequiresCharging(true)      //僅在設備充電時運行
                .setRequiresBatteryNotLow(true) //指示僅當電池電量不足時才應運行工作請求
                .apply {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setRequiresDeviceIdle(true)
                    }
                }
                .build()        //從構建器生成約束

        //創建並初始化一個定期工作請求，使其每天運行一次PeriodicWorkRequestBuilder()
        //傳遞RefreshDataWorker您在上一個任務中創建的類。1以時間單位為重複間隔為TimeUnit.DAYS
        val repeatingRequest = PeriodicWorkRequestBuilder<RefreshDataWorker>(1, TimeUnit.DAYS)
                .setConstraints(constraints)
                .build()

        Timber.d("WorkManager: Periodic Work request for sync is scheduled")
        //傳遞KEEP枚舉ExistingPeriodicWorkPolicy。repeatingRequest作為PeriodicWorkRequest參數傳遞
        WorkManager.getInstance().enqueueUniquePeriodicWork(
                RefreshDataWorker.WORK_NAME,
                ExistingPeriodicWorkPolicy.KEEP,
                repeatingRequest)


    }
}
