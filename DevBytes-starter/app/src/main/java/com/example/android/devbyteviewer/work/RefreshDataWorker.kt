package com.example.android.devbyteviewer.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.android.devbyteviewer.database.getDatabase
import com.example.android.devbyteviewer.repository.VideosRepository
import retrofit2.HttpException
import timber.log.Timber

//RefreshDataWorker從CoroutineWorker課程中擴展課程。傳入context和WorkerParameters作為構造函數參數
class RefreshDataWorker(appContext: Context,params: WorkerParameters) : CoroutineWorker(appContext,params) {
    override suspend fun doWork(): Result {
        val database = getDatabase(applicationContext)
        val repository = VideosRepository(database)
        try {
            repository.refreshVideos()
        } catch (e: HttpException) {
            return Result.retry()
        }
        return Result.success()
    }
    companion object {
        //定義工作名稱以唯一標識此工作人員
        const val WORK_NAME = "com.example.android.devbyteviewer.work.RefreshDataWorker"
    }
}